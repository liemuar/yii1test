<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */
if(!Yii::app()->user->isGuest){
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); } else{
	$this->redirect(Yii::app()->user->returnUrl);
} ?>
